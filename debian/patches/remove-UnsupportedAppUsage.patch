Description: Remove UnsupportedAppUsage annotation
 UnsupportedAppUsage is an annotation that is used to specify that the class
 or object used isn't techically a part of the Android SDK and thus isn't
 supported. It is more of a warning for ourselves. It can be safely neglected.
Author: Raman Sarda <theloudspeaker@disroot.org>
Last-Update: 2020-05-18

--- a/json/src/main/java/org/json/JSONArray.java
+++ b/json/src/main/java/org/json/JSONArray.java
@@ -16,7 +16,6 @@
 
 package org.json;
 
-import dalvik.annotation.compat.UnsupportedAppUsage;
 import java.lang.reflect.Array;
 import java.util.ArrayList;
 import java.util.Collection;
@@ -49,7 +48,6 @@ import java.util.List;
  */
 public class JSONArray {
 
-    @UnsupportedAppUsage
     private final List<Object> values;
 
     /**
@@ -609,7 +607,6 @@ public class JSONArray {
         return stringer.toString();
     }
 
-    @UnsupportedAppUsage
     void writeTo(JSONStringer stringer) throws JSONException {
         stringer.array();
         for (Object value : values) {
--- a/json/src/main/java/org/json/JSONObject.java
+++ b/json/src/main/java/org/json/JSONObject.java
@@ -16,7 +16,6 @@
 
 package org.json;
 
-import dalvik.annotation.compat.UnsupportedAppUsage;
 import java.util.ArrayList;
 import java.util.Collection;
 import java.util.Iterator;
@@ -83,7 +82,6 @@ import libcore.util.Nullable;
  */
 public class JSONObject {
 
-    @UnsupportedAppUsage
     private static final Double NEGATIVE_ZERO = -0d;
 
     /**
@@ -112,7 +110,6 @@ public class JSONObject {
         }
     };
 
-    @UnsupportedAppUsage
     private final LinkedHashMap<String, Object> nameValuePairs;
 
     /**
@@ -333,7 +330,6 @@ public class JSONObject {
      *
      * @hide
      */
-    @UnsupportedAppUsage
     public JSONObject append(String name, Object value) throws JSONException {
         Object current = nameValuePairs.get(checkName(name));
 
@@ -353,7 +349,6 @@ public class JSONObject {
         return this;
     }
 
-    @UnsupportedAppUsage
     String checkName(String name) throws JSONException {
         if (name == null) {
             throw new JSONException("Names must be non-null");
@@ -675,7 +670,6 @@ public class JSONObject {
      *
      * @hide.
      */
-    @UnsupportedAppUsage
     @libcore.api.CorePlatformApi
     public Set<String> keySet() {
         return nameValuePairs.keySet();
@@ -726,7 +720,6 @@ public class JSONObject {
         return stringer.toString();
     }
 
-    @UnsupportedAppUsage
     void writeTo(JSONStringer stringer) throws JSONException {
         stringer.object();
         for (Map.Entry<String, Object> entry : nameValuePairs.entrySet()) {
--- a/json/src/main/java/org/json/JSONStringer.java
+++ b/json/src/main/java/org/json/JSONStringer.java
@@ -16,7 +16,6 @@
 
 package org.json;
 
-import dalvik.annotation.compat.UnsupportedAppUsage;
 import java.util.ArrayList;
 import java.util.Arrays;
 import java.util.List;
@@ -62,7 +61,6 @@ import java.util.List;
 public class JSONStringer {
 
     /** The output data, containing at most one top-level array or object. */
-    @UnsupportedAppUsage
     final StringBuilder out = new StringBuilder();
 
     /**
@@ -113,21 +111,18 @@ public class JSONStringer {
      * Unlike the original implementation, this stack isn't limited to 20
      * levels of nesting.
      */
-    @UnsupportedAppUsage
     private final List<Scope> stack = new ArrayList<Scope>();
 
     /**
      * A string containing a full set of spaces for a single level of
      * indentation, or null for no pretty printing.
      */
-    @UnsupportedAppUsage
     private final String indent;
 
     public JSONStringer() {
         indent = null;
     }
 
-    @UnsupportedAppUsage
     JSONStringer(int indentSpaces) {
         char[] indentChars = new char[indentSpaces];
         Arrays.fill(indentChars, ' ');
@@ -176,7 +171,6 @@ public class JSONStringer {
      * Enters a new scope by appending any necessary whitespace and the given
      * bracket.
      */
-    @UnsupportedAppUsage
     JSONStringer open(Scope empty, String openBracket) throws JSONException {
         if (stack.isEmpty() && out.length() > 0) {
             throw new JSONException("Nesting problem: multiple top-level roots");
@@ -191,7 +185,6 @@ public class JSONStringer {
      * Closes the current scope by appending any necessary whitespace and the
      * given bracket.
      */
-    @UnsupportedAppUsage
     JSONStringer close(Scope empty, Scope nonempty, String closeBracket) throws JSONException {
         Scope context = peek();
         if (context != nonempty && context != empty) {
@@ -209,7 +202,6 @@ public class JSONStringer {
     /**
      * Returns the value on the top of the stack.
      */
-    @UnsupportedAppUsage
     private Scope peek() throws JSONException {
         if (stack.isEmpty()) {
             throw new JSONException("Nesting problem");
@@ -220,7 +212,6 @@ public class JSONStringer {
     /**
      * Replace the value on the top of the stack with the given value.
      */
-    @UnsupportedAppUsage
     private void replaceTop(Scope topOfStack) {
         stack.set(stack.size() - 1, topOfStack);
     }
@@ -308,7 +299,6 @@ public class JSONStringer {
         return this;
     }
 
-    @UnsupportedAppUsage
     private void string(String value) {
         out.append("\"");
         for (int i = 0, length = value.length(); i < length; i++) {
@@ -360,7 +350,6 @@ public class JSONStringer {
         out.append("\"");
     }
 
-    @UnsupportedAppUsage
     private void newline() {
         if (indent == null) {
             return;
@@ -391,7 +380,6 @@ public class JSONStringer {
      * Inserts any necessary separators and whitespace before a name. Also
      * adjusts the stack to expect the key's value.
      */
-    @UnsupportedAppUsage
     private void beforeKey() throws JSONException {
         Scope context = peek();
         if (context == Scope.NONEMPTY_OBJECT) { // first in object
@@ -408,7 +396,6 @@ public class JSONStringer {
      * inline array, or inline object. Also adjusts the stack to expect either a
      * closing bracket or another element.
      */
-    @UnsupportedAppUsage
     private void beforeValue() throws JSONException {
         if (stack.isEmpty()) {
             return;
--- a/json/src/main/java/org/json/JSONTokener.java
+++ b/json/src/main/java/org/json/JSONTokener.java
@@ -16,7 +16,6 @@
 
 package org.json;
 
-import dalvik.annotation.compat.UnsupportedAppUsage;
 
 // Note: this class was written without inspecting the non-free org.json sourcecode.
 
@@ -64,14 +63,12 @@ import dalvik.annotation.compat.Unsuppor
 public class JSONTokener {
 
     /** The input JSON. */
-    @UnsupportedAppUsage
     private final String in;
 
     /**
      * The index of the next character to be returned by {@link #next}. When
      * the input is exhausted, this equals the input's length.
      */
-    @UnsupportedAppUsage
     private int pos;
 
     /**
@@ -116,7 +113,6 @@ public class JSONTokener {
         }
     }
 
-    @UnsupportedAppUsage
     private int nextCleanInternal() throws JSONException {
         while (pos < in.length()) {
             int c = in.charAt(pos++);
@@ -176,7 +172,6 @@ public class JSONTokener {
      * is terminated by "\r\n", the '\n' must be consumed as whitespace by the
      * caller.
      */
-    @UnsupportedAppUsage
     private void skipToEndOfLine() {
         for (; pos < in.length(); pos++) {
             char c = in.charAt(pos);
@@ -240,7 +235,6 @@ public class JSONTokener {
      * been read. This supports both unicode escapes "u000A" and two-character
      * escapes "\n".
      */
-    @UnsupportedAppUsage
     private char readEscapeCharacter() throws JSONException {
         char escaped = in.charAt(pos++);
         switch (escaped) {
@@ -284,7 +278,6 @@ public class JSONTokener {
      * values will be returned as an Integer, Long, or Double, in that order of
      * preference.
      */
-    @UnsupportedAppUsage
     private Object readLiteral() throws JSONException {
         String literal = nextToInternal("{}[]/\\:,=;# \t\f");
 
@@ -339,7 +332,6 @@ public class JSONTokener {
      * Returns the string up to but not including any of the given characters or
      * a newline character. This does not consume the excluded character.
      */
-    @UnsupportedAppUsage
     private String nextToInternal(String excluded) {
         int start = pos;
         for (; pos < in.length(); pos++) {
@@ -355,7 +347,6 @@ public class JSONTokener {
      * Reads a sequence of key/value pairs and the trailing closing brace '}' of
      * an object. The opening brace '{' should have already been read.
      */
-    @UnsupportedAppUsage
     private JSONObject readObject() throws JSONException {
         JSONObject result = new JSONObject();
 
@@ -411,7 +402,6 @@ public class JSONTokener {
      * "[]" yields an empty array, but "[,]" returns a two-element array
      * equivalent to "[null,null]".
      */
-    @UnsupportedAppUsage
     private JSONArray readArray() throws JSONException {
         JSONArray result = new JSONArray();
 
